//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TiendaVirtualWeb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cliente()
        {
            this.Pedidoes = new HashSet<Pedido>();
        }
    
        public int Id { get; set; }
        public string cli_identificacion { get; set; }
        public string cli_nombre { get; set; }
        public string cli_apeliido { get; set; }
        public string cli_direccion { get; set; }
        public string cli_movil { get; set; }
        public System.DateTime cli_fecha_registro { get; set; }
        public string usu_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pedido> Pedidoes { get; set; }
    }
}
