﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using TiendaVirtualWeb.Models;

[assembly: OwinStartupAttribute(typeof(TiendaVirtualWeb.Startup))]
namespace TiendaVirtualWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createUserAdmin();
        }

        private void createUserAdmin()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var userAdmin = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleAdmin = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!roleAdmin.RoleExists(Constantes.rolAdmin))
            {
                var role = new IdentityRole();
                role.Name = Constantes.rolAdmin;
                roleAdmin.Create(role);

                //Here we create a Admin super user who will maintain the website                  

                var user = new ApplicationUser();
                user.UserName = Constantes.userAdmin;
                user.Email = Constantes.userAdmin;
                string userPassword = Constantes.passAdmin;
                var chkUser = userAdmin.Create(user, userPassword);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    userAdmin.AddToRole(user.Id, Constantes.rolAdmin);
                }

            }
        }

    }
}
