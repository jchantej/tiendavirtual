﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TiendaVirtualWeb
{
    public static class Constantes
    {
        /*Volor que permite identificar cuando un producto esta Eliminado*/
        public static int productoEliminado = -1;

        /*Rol Admin por defecto*/
        public static string rolAdmin = "admin";
        /*Usuario Administrador por defecto*/
        public static string userAdmin = "admin@admin.com";
        public static string passAdmin = "admin123";
    }
}