﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TiendaVirtualWeb;

namespace TiendaVirtualWeb.Controllers
{
    [Authorize]
    public class ClientesController : Controller
    {
        private TiendaVirtualEntities db = new TiendaVirtualEntities();

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,cli_identificacion,cli_nombre,cli_apeliido,cli_direccion,cli_movil")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                cliente.cli_fecha_registro = DateTime.Now;
                cliente.usu_id = User.Identity.Name;
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("ListarProdutoCarro", "Productos");
            }

            return View(cliente);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
