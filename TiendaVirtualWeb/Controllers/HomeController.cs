﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TiendaVirtualWeb.Models;

namespace TiendaVirtualWeb.Controllers
{
    public class HomeController : Controller
    {
        private TiendaVirtualEntities db = new TiendaVirtualEntities();
        dynamic carroCompraModel = new ExpandoObject();
        public ActionResult Index(string busqueda, CarroCompra carroCompra)
        {
            if (busqueda == null || busqueda == "")
            {
                carroCompraModel.Productos = db.Productoes.ToList().FindAll(p => p.pro_cantidad > Constantes.productoEliminado);
            }
            else
            {

                carroCompraModel.Productos = db.Productoes.ToList().FindAll(
                        p => p.pro_cantidad > Constantes.productoEliminado &&
                         (p.pro_nombre.ToLower().Contains(busqueda)
                        || p.pro_descripcion.ToLower().Contains(busqueda)
                        || p.pro_categoria.ToLower().Contains(busqueda)
                        || p.pro_autor.ToLower().Contains(busqueda)
                        || p.pro_isbn.ToLower().Contains(busqueda)));
            }

            carroCompraModel.ProductosCarro = carroCompra;
            return View(carroCompraModel);


        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}