﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TiendaVirtualWeb.Models;

namespace TiendaVirtualWeb.Controllers
{

   [Authorize]
    public class PedidosController : Controller
    {
        private TiendaVirtualEntities db = new TiendaVirtualEntities();
        dynamic carroCompraModel = new ExpandoObject();

        // GET: Pedidos
        public async Task<ActionResult> Index(CarroCompra carroCompra)
        {
            string hostPort = Request.Url.Authority;
            int idCLiente = 0;
            string UrlWepApi = Request.Url.Scheme + "://" + hostPort + "/api/tienda/pedidos";
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(UrlWepApi);
            var pedidos = await response.Content.ReadAsAsync<IEnumerable<Pedido>>();
            if ((db.Clientes.ToList().Exists(c => c.usu_id == User.Identity.Name)))
            {
                idCLiente = (from c in db.Clientes
                             where c.usu_id == User.Identity.Name
                             select c.Id).First();
                carroCompraModel.Cliente = db.Clientes.ToList().FindAll(c => c.usu_id == User.Identity.Name).First();
            }
            else {
                carroCompraModel.Cliente = null;
            }

            carroCompraModel.Pedidos = pedidos.ToList().FindAll(p => p.cli_id == idCLiente);
            carroCompraModel.ProductosCarro = carroCompra;
     
            return View(carroCompraModel);

        }


        // GET: Pedidos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Pedidos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pedidos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pedidos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Pedidos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Pedidos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Pedidos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
