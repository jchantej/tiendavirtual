﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TiendaVirtualWeb;
using TiendaVirtualWeb.Models;

namespace TiendaVirtualWeb.Controllers
{

    public class ProductosController : Controller
    {
        private TiendaVirtualEntities db = new TiendaVirtualEntities();
        dynamic carroCompraModel = new ExpandoObject();


        // GET: Productos
        [Authorize(Roles = "admin")]
        public ActionResult Index(CarroCompra carroCompra)
        {
            carroCompraModel.Productos = db.Productoes.ToList();
            carroCompraModel.ProductosCarro = carroCompra;
            return View(carroCompraModel);

        }

        // GET: Productos/Details/5
        [Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productoes.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Productos/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,pro_nombre,pro_cantidad,pro_precio,pro_descripcion,por_imagen,pro_categoria,pro_autor,pro_isbn")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                producto.pro_fecha_registro = DateTime.Now;
                db.Productoes.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        // GET: Productos/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productoes.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,pro_nombre,pro_cantidad,pro_precio,pro_descripcion,por_imagen,pro_categoria,pro_autor,pro_isbn")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                producto.pro_fecha_registro = DateTime.Now;
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(producto);
        }

        // GET: Productos/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productoes.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productoes.Find(id);
            producto.pro_cantidad = Constantes.productoEliminado;
            db.Entry(producto).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddProdutoCarro(int id, CarroCompra carroCompra)
        {
            int contadorProductos = 1;
            Producto producto = db.Productoes.Find(id);
            if ((carroCompra.Count != 0) && (carroCompra.Exists(p => p.Id == id)))
            {
                if (producto.pro_cantidad > carroCompra.Find(p => p.Id == id).pro_cantidad)
                {
                    carroCompra.Find(p => p.Id == id).pro_cantidad += contadorProductos;
                }

            }
            else
            {
                if (producto.pro_cantidad > 0)
                {
                    carroCompra.Add(producto);
                    carroCompra.Find(p => p.Id == id).pro_cantidad = contadorProductos;
                }

            }

            return RedirectToAction("Index", "Home");

        }
        public ActionResult RemoverProdutoCarro(int id, CarroCompra carroCompra)
        {
            carroCompra.RemoveAll(p => p.Id == id);
            return RedirectToAction("ListarProdutoCarro");

        }
        public ActionResult ListarProdutoCarro(CarroCompra carroCompra)
        {

            carroCompraModel.ProductosCarro = carroCompra;
            carroCompraModel.Productos = db.Productoes.ToList();
            return View(carroCompraModel);

        }
        public ActionResult GenerarPedido(CarroCompra carroCompra)
        {
            /*TODO: De momento datos quemados para cliente
             */
            if (User.Identity.IsAuthenticated)
            {
                Random rnd = new Random();
                String valor = Convert.ToString(rnd.Next(0, 20));
                Pedido pedido = new Pedido();
                List<ProductoPedido> productosPedidos = new List<ProductoPedido>();
                List<Producto> productos = new List<Producto>();
                Cliente cliente = new Cliente();
                Producto producto = new Producto();
                decimal precioTotalCompra = 0;
                if ((db.Clientes.ToList().Exists(c => c.usu_id == User.Identity.Name)))
                {
                    cliente =
                            (from c in db.Clientes.ToList()
                            where c.usu_id == User.Identity.Name
                            select c).First();

                }
                else
                {
                    return RedirectToAction("Create", "Clientes");

                }

                pedido.ped_estado = 1;
                pedido.ped_fecha_registro = DateTime.Now;
                pedido.Cliente = cliente;
                pedido.ped_total_productos = carroCompra.Sum(p => p.pro_cantidad);
                foreach (var item in carroCompra)
                {
                    ProductoPedido productoPedido = new ProductoPedido();
                    Producto productoTemp = db.Productoes.Find(item.Id);
                    productoTemp.pro_cantidad -= item.pro_cantidad;
                    precioTotalCompra += item.pro_cantidad * item.pro_precio;
                    productoPedido.Pedido = pedido;
                    productoPedido.pro_id = item.Id;
                    productoPedido.prp_catidad_producto = item.pro_cantidad;
                    productoPedido.prp_fecha_registro = DateTime.Now;
                    db.ProductoPedidoes.Add(productoPedido);
                    db.Entry(productoTemp).State = EntityState.Modified;
                }

                pedido.ped_monto = precioTotalCompra;
                db.Pedidoes.Add(pedido);
                db.SaveChanges();
                carroCompra.Clear();
                carroCompraModel.ProductosCarro = carroCompra;
                return RedirectToAction("Index", "Pedidos");
            }
           
            return RedirectToAction("Login", "Account");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
