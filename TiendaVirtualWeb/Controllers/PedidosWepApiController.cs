﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TiendaVirtualWeb;

namespace TiendaVirtualWeb.Controllers
{

    public class PedidosWepApiController : ApiController
    {

        private TiendaVirtualEntities db = new TiendaVirtualEntities();

        // GET: api/PedidosWepApi
        [Route("api/tienda/pedidos")]
        public IQueryable<Pedido> GetPedidoes()
        {
            return db.Pedidoes;
        }

        // GET: api/PedidosWepApi/5
        [Route("api/tienda/pedidos/{id}")]
        [ResponseType(typeof(Pedido))]
        public IHttpActionResult GetPedido(int id)
        {
            Pedido pedido = db.Pedidoes.Find(id);
            if (pedido == null)
            {
                return NotFound();
            }

            return Ok(pedido);
        }
    }
}