﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TiendaVirtualWeb.Models.ModelsBinder
{
    public class CarroCompraModelBinder: IModelBinder
    {
        private string key_carro_session = "KEY_CARRO_SESION";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext modelBindigContext)
        {
            CarroCompra _carroCompra = (CarroCompra)controllerContext.HttpContext.Session[key_carro_session];
            if (_carroCompra == null)
            {
                _carroCompra = new CarroCompra();
                controllerContext.HttpContext.Session[key_carro_session] = _carroCompra;
            }
            return _carroCompra;
        }
    }
}