﻿if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('catalago') and o.name = 'fk_catalago_catalogo_catalago')
alter table catalago
   drop constraint fk_catalago_catalogo_catalago
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('catalago')
            and   name  = 'catalogo_fk'
            and   indid > 0
            and   indid < 255)
   drop index catalago.catalogo_fk
go

if exists (select 1
            from  sysobjects
           where  id = object_id('catalago')
            and   type = 'U')
   drop table catalago
go

/*==============================================================*/
/* Table: catalago                                              */
/*==============================================================*/
create table Catalago (
   Id               int                  not null,
   cat_cat_id           int                  null,
   cat_codigo_padre     varchar(50)          not null,
   cat_codigo_hijo      varchar(50)          not null,
   cat_nombre           varchar(50)          not null,
   cat_descripcion      varchar(100)         not null,
   cat_fecha            datetime             null,
   constraint pk_catalago primary key (Id)
)
go

/*==============================================================*/
/* Index: catalogo_fk                                           */
/*==============================================================*/




create nonclustered index catalogo_fk on catalago (cat_cat_id asc)
go

alter table catalago
   add constraint fk_catalago_catalogo_catalago foreign key (cat_cat_id)
      references catalago (Id)
go
